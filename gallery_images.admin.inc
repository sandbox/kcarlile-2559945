<?php
/**
  * @file
  * Supporting code for admin functionality.
  */

// Set to TRUE for admin page debugging functions
define("ADMIN_DEBUG", TRUE);

function gallery_images_config($form, &$form_state) {
  $form = array();

  // TODO: file save path
  
  if (ADMIN_DEBUG) {
    $form['break_1'] = array(
        '#markup'     => '<hr /><br />',
    );
    
    $form['reload_content_types'] = array(
      '#type'         => 'submit',
      '#value'        => t('Reload Content Types'),
      '#submit'       => array('gallery_images_reload_content_types'),
    );
  }
  
  return $form;
}

function gallery_images_reload_content_types($form, &$form_state) {
  // Load node.admin.inc from the node module.
  module_load_include('install', 'gallery_images', 'gallery_images');
  gallery_images_uninstall();
  drupal_set_message('GI content types uninstalled');
  gallery_images_install();
  drupal_set_message('GI content types re-installed');

} // end function sd_configurator_actions_flush_load_validate