<?php
/**
 * @file
 * Contains functions for creating content types used by Gallery Images module.
 */


/*
// ---------- Gallery Image ----------
*/

function _gallery_images_create_gallery_image_type() {
  // During installation, the t() function is unavailable, so we use get_t()
  // to store the name of the translation function.
  $t = get_t();

  // We define the node type as an associative array.
  $gallery_image = array(
    'type' => 'gallery_image',
    'name' => $t('Gallery Image'),
    // 'base' tells Drupal the base string for hook functions.
    // This is often the module name; if base is set to 'mymodule', Drupal
    // would call mymodule_insert() or similar for node hooks.
    // In this case, we set base equal to 'node_content' so Drupal will handle
    // our node as if we had designed it in the UI.
    'base' => 'node_content',
    'description' => $t('Gallery Image nodes.'),
    'title_label' => $t('Gallery Image'),
    'custom' => TRUE,
  );

  // Complete the node type definition by setting any defaults not explicitly
  // declared above.
  // http://api.drupal.org/api/function/node_type_set_defaults/7
  $content_type = node_type_set_defaults($gallery_image);

  //We add a body field and set the body label immediately.
  //node_add_body_field($content_type, $t('Example Description'));

  // Save the content type
  node_type_save($content_type);

  drupal_set_message('GI content type created');

  // Create all the fields we are adding to our content type.
  // http://api.drupal.org/api/function/field_create_field/7
  foreach (_gallery_image_installed_fields() as $field) {
    field_create_field($field);
  }

  drupal_set_message('GI fields created');

  // Create all the instances for our fields.
  // http://api.drupal.org/api/function/field_create_instance/7
  foreach (_gallery_image_installed_instances() as $instance) {
    $instance['entity_type'] = 'node';
    $instance['bundle'] = $gallery_image['type'];
    field_create_instance($instance);
  }

  drupal_set_message('GI instances created');
}

/**
  * Returns a structured array defining the fields created by this content type.
  *
  * This is factored into this function so it can be used in both
  * gallery_images_install() and gallery_images_uninstall().
  *
  * @return
  *  An associative array specifying the fields we wish to add to our
  *  new node type.
  */
function _gallery_image_installed_fields() {
  // http://api.drupal.org/api/drupal/modules!field!field.module/group/field/7
  $t = get_t();

  // title (auto)
  // description
  // tag
  // node ref images
  // published

  $fields = array(
    // http://ado.io/blog/drupal-7/working-programmatically-with-fields-in-drupal-7
    'field_gi_description' => array(
      'field_name'  => 'field_gi_description',
      'type'        => 'text_long',
      'cardinality' => 1, // number of values this field can hold
      'settings'    => array(),
    ),
    'field_gi_exif' => array(
      'field_name'  => 'field_gi_exif',
      'type'        => 'text_long',
      'cardinality' => 1, // number of values this field can hold
      'settings'    => array(),
    ),
  );

  return $fields;
}

/**
  * Returns a structured array defining the instances for this content type.
  *
  * This is factored into this function so it can be used in both
  * gallery_images_install() and gallery_images_uninstall().
  *
  * @return
  *  An associative array specifying the instances we wish to add to our new
  *  node type.
  */
function _gallery_image_installed_instances() {
  $t = get_t();

  return array(
    'field_gi_description' => array(
      'field_name'  => 'field_gi_description',
      'label'       => $t('Description'),
      'description' => $t('Text describing the image.'),
      'required'    => TRUE,
      'widget'      => array(
        'type'      => 'text_textarea',
        'settings'  => array(
          'size'    => 500,
        ),
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
      'weight' => 1,
      'display' => array(
        'field_gic_description' => array(
          'label' => 'above',
        ),
      ),
    ),
    'field_gi_exif' => array(
      'field_name'  => 'field_gi_exif',
      'label'       => $t('EXIF Data'),
      'description' => $t('EXIF data from the image.'),
      'required'    => TRUE,
      'widget'      => array(
        'type'      => 'text_textarea',
        'settings'  => array(
          'size'    => 500,
        ),
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
      'weight' => 1,
      'display' => array(
        'field_gic_description' => array(
          'label' => 'above',
        ),
      ),
    ),
  );
}

/*
// ---------- Gallery Images Collection ----------
*/

/**
  * Performs the functions to create the Gallery Images Collection content type.
  */
function _gallery_images_create_gallery_images_collection_type() {
// During installation, the t() function is unavailable, so we use get_t()
// to store the name of the translation function.
$t = get_t();

// We define the node type as an associative array.
$gallery_images_collection = array(
  'type' => 'gallery_images_collection',
  'name' => $t('Gallery Images Collection'),
  // 'base' tells Drupal the base string for hook functions.
  // This is often the module name; if base is set to 'mymodule', Drupal
  // would call mymodule_insert() or similar for node hooks.
  // In this case, we set base equal to 'node_content' so Drupal will handle
  // our node as if we had designed it in the UI.
  'base' => 'node_content',
  'description' => $t('Collection of Gallery Image nodes, like a gallery.'),
  'title_label' => $t('Gallery Images Collection'),
  'custom' => TRUE,
);

// Complete the node type definition by setting any defaults not explicitly
// declared above.
// http://api.drupal.org/api/function/node_type_set_defaults/7
$content_type = node_type_set_defaults($gallery_images_collection);

//We add a body field and set the body label immediately.
//node_add_body_field($content_type, $t('Example Description'));

// Save the content type
node_type_save($content_type);

drupal_set_message('GIC content type created');

// Create all the fields we are adding to our content type.
// http://api.drupal.org/api/function/field_create_field/7
foreach (_gallery_images_collection_installed_fields() as $field) {
  field_create_field($field);
}


drupal_set_message('GIC fields created');

// Create all the instances for our fields.
// http://api.drupal.org/api/function/field_create_instance/7
foreach (_gallery_images_collection_installed_instances() as $instance) {
  $instance['entity_type'] = 'node';
  $instance['bundle'] = $gallery_images_collection['type'];
  field_create_instance($instance);
}

drupal_set_message('GIC instances created');
}


/**
  * Returns a structured array defining the fields created by this content type.
  *
  * This is factored into this function so it can be used in both
  * gallery_images_install() and gallery_images_uninstall().
  *
  * @return
  *  An associative array specifying the fields we wish to add to our
  *  new node type.
  */
function _gallery_images_collection_installed_fields() {
  // http://api.drupal.org/api/drupal/modules!field!field.module/group/field/7
  $t = get_t();

  // title (auto)
  // description
  // tag
  // node ref images
  // published

  $fields = array(
    // http://ado.io/blog/drupal-7/working-programmatically-with-fields-in-drupal-7
    'field_gic_description' => array(
      'field_name'  => 'field_gic_description',
      'type'        => 'text_long',
      'cardinality' => 1, // number of values this field can hold
      'settings'    => array(),
    ),
  );

  return $fields;
}

/**
  * Returns a structured array defining the instances for this content type.
  *
  * This is factored into this function so it can be used in both
  * gallery_images_install() and gallery_images_uninstall().
  *
  * @return
  *  An associative array specifying the instances we wish to add to our new
  *  node type.
  */
function _gallery_images_collection_installed_instances() {
  $t = get_t();

  return array(
    'field_gic_description' => array(
      'field_name'  => 'field_gic_description',
      'label'       => $t('Description'),
      'description' => $t('Text describing the collection.'),
      'required'    => TRUE,
      'widget'      => array(
        'type'      => 'text_textarea',
        'settings'  => array(
          'size'    => 500,
        ),
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
      'weight' => 1,
      'display' => array(
        'field_gic_description' => array(
          'label' => 'above',
        ),
      ),
    ),
  );
}