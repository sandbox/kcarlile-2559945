<?php
/**
  * @file
  * Supporting code for Gallery Images collection functionality.
  */

/**
  * Implements hook_form_alter().
  */
//function gallery_images_form_alter(&$form, $form_state, $form_id) {
function gallery_images_form_gallery_images_collection_node_form_alter(&$form, $form_state, $form_id) {
  // http://mohitaghera.drupalgardens.com/content/multiple-file-upload-usind-drupal-7-form-api
  $form['gallery_images_files'] = array(
    '#type' => 'file',
    '#name' => 'files[]',
    '#title' => t('Gallery Images'),
    '#description' => t('Allowed file tyopes: jpg, jpeg, png, and gif (10MB Max Size)'),
    '#attributes' => array(
      'multiple' => 'multiple'
    ),
  );
  $form['#validate'][] = 'gallery_images_form_gallery_images_collection_node_form_validate';
}

function gallery_images_form_gallery_images_collection_node_form_validate($form, &$form_state) {
  drupal_set_message('Validatimasizing...');
  $num_files = count($_FILES['files']['name']);
  for ($i = 0; $i < $num_files; $i++) {
    $file = file_save_upload($i, array(
      'file_validate_is_image' => array(),
      'file_validate_extensions' => array('png gif jpg jpeg'),
    ));

    drupal_set_message(print_r($file, true));

    if ($file) {
      if ($file = file_move($file, 'public://')) { // TODO: configure path via admin
        $form_state['values']['file'][$i] = $file;
      }
      else {
        form_set_error('file', t('Failed to write the uploaded file the site\'s file folder.'));
      }
    }
    else {
      form_set_error('file', t('No file was uploaded.'));
    }
  }
}

// TODO: implement form submit action to create image nodes and create node references
// http://mohitaghera.drupalgardens.com/content/multiple-file-upload-usind-drupal-7-form-api